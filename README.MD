# ETV Site: Teacher of the year election #
## Introduction ##

This project belongs to the website of the election of the Teacher of the year, organised by the ETV in cooperation with KH. This site (https://toty.etv.tudelft.nl) will gather all votes of the ETV members.

## Functionality requirements
 - ### Landing page:
 A landing page for everyone, giving information about the event e.g. time and date. A user also has to have the option to resend the email with their personal voting link.

 - ### Voting page:
 A page for user who received an email with a unique link to place their votes. 

 - ### Admin page:
 A page to see the current votes, enable and disable voting, and exporting vote results.

## Technical information

### Yii2
The project is build in the Yii2 PHP framework, for more info about Yii2, see their [online documentation](http://www.yiiframework.com/doc-2.0/). If you are not fammiliar with Yii2, we recommend you to follow their [online guide](http://www.yiiframework.com/doc-2.0/guide-index.html).

### Database information
The data needed for this project is:

- ETV members (name, email and ID)
- Teachers (Name, department etc)
- Courses (To find the correct teachers for voter

You are free to choose your database structure, the HoCo will take care of filling the DB on the production server with the propper queries for members, teachers en courses, following the database structure provided in the latest migration.

### Migrations
The complete database structure has to be described in migrations. In this way we can easily manage the database structure.

### Code conventions:
- All opening curly brackets inline (including class opening brackets)!
- `foreach ($item in $array) { }` in controllers, `foreach ($item in $array) : endforeach;` in views
- `<?= $var ?>` instead of `<?php echo $var ?>` is OK
- ommit PHP end tag (`?>`) in PHP files
- all variable names in English, also in the database
- tabelnames and columnames, lowercase, snakecase
- php class names uppercamelcase (Pascal case)
- php function and variable names in camelcase
- Models: table names in plural, model names in singular
- private variables begin with underscore


### Git conventions ###
- create feature branches as `feature/issuenumber-featurename`

### How to make things work
- `composer install`
- fix Yii Migration if necessary
  - possible fix for `db_console.php`: change localhost to 127.0.0.1 and possibly add custom MySQL port
  - `php yii migrate` to apply newer migrations
  - `php yii migrate/down` to undo all migrations
  - `php yii migrate/create create_news_table` to generate a new migration in `migrations` directory

## Project manager ##
 - Erné Bronkhorst

## Contributor(s) ##
 - Erné Bronkhorst
